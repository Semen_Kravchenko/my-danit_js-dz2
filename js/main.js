//Ответ на теорию:
//В программированнии циклы нужны для того, чтобы выполнить ту или иную операцию определенное или неопределенное количество раз, то есть они позволяют не описывать каждую операцию вручную.
//Это удобно, когда необходимо дублировать операцию несколько раз подряд, или перебрать определенную последовательность чисел в заданном диапозоне, ну и т.д...

//Практика:

let num;
let primeNumbers;
let m;
let n;

do{
    num = prompt(`Enter your number: `);
    if (num === parseInt(num, 10)){
        break;
    }
}
while(num % 1 !== 0);

for (let i = 0; i <= num; i++) {
    if (num < 5) {
        console.log(`Sorry, no numbers`);
        break;
    }
    else if (i % 5 === 0) {
        console.log(i);
    }
}

primeNumbers = confirm (`Go to the advanced task?`);
if (primeNumbers === true) {
    m = +prompt(`Enter first number: `);
    n = +prompt(`Enter second number: `);

    if (m <= 1 || m >= n ||
        isNaN(m) || isNaN(n) ||
        m === null || n === null ||
        m === undefined || n === undefined ||
        m === "" || n === "") {

        while (m <= 1 || m >= n ||
                isNaN(m) || isNaN(n) ||
                m === null || n === null ||
                m === undefined || n === undefined ||
                m === "" || n === "") {
            alert(`Error`);
            m = +prompt(`Enter first number again: `);
            n = +prompt(`Enter second number again: `);
        }
    }
//*******************************************
    nextPrime:
    for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j == 0) continue nextPrime;
    }
    console.log(`${i} is prime number`);
    }
//*******************************************
}
//P.S.: ^Данный участок кода был скопипастен, и мною немного адаптирован)). Всё остальное моё.
